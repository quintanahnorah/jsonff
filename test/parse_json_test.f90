module parse_json_test
    use erloff, only: ErrorList_t
    use iso_varying_string, only: VARYING_STRING, operator(//), var_str
    use jsonff, only: &
            JsonArray_t, &
            JsonElement_t, &
            JsonFalse_t, &
            JsonNull_t, &
            JsonNumber_t, &
            JsonObject_t, &
            JsonString_t, &
            JsonTrue_t, &
            parseJson
    use strff, only: NEWLINE
    use Vegetables_m, only: &
            Example_t, &
            Input_t, &
            Result_t, &
            TestItem_t, &
            assertEquals, &
            assertNot, &
            assertThat, &
            describe, &
            Example, &
            fail, &
            it, &
            succeed

    implicit none
    private

    type, extends(Input_t) :: NumberInput_t
        type(VARYING_STRING) :: string
        double precision :: value_
    end type NumberInput_t

    public :: test_parse_json
contains
    function test_parse_json() result(tests)
        type(TestItem_t) :: tests

        type(TestItem_t) :: individual_tests(13)
        type(Example_t) :: number_examples(7)

        number_examples(1) = Example(NumberInput_t(var_str("0"), 0.0d0))
        number_examples(2) = Example(NumberInput_t(var_str("2"), 2.0d0))
        number_examples(3) = Example(NumberInput_t(var_str("-2.0"), -2.0d0))
        number_examples(4) = Example(NumberInput_t(var_str("0.2"), 0.2d0))
        number_examples(5) = Example(NumberInput_t(var_str("1.2e3"), 1.2d3))
        number_examples(6) = Example(NumberInput_t(var_str("1.2E+3"), 1.2d3))
        number_examples(7) = Example(NumberInput_t(var_str("1.2e-3"), 1.2d-3))

        individual_tests(1) = it( &
                "parsing an empty string returns an error", checkParseEmpty)
        individual_tests(2) = it("can parse null", checkParseNull)
        individual_tests(3) = it("can parse true", checkParseTrue)
        individual_tests(4) = it("can parse false", checkParseFalse)
        individual_tests(5) = it( &
                "can parse a variety of numbers", &
                number_examples, &
                checkParseNumber)
        individual_tests(6) = it("can parse a string", checkParseString)
        individual_tests(7) = it( &
                "can parse an empty array", checkParseEmptyArray)
        individual_tests(8) = it( &
                "can parse an array with a single element", &
                checkParseSingleArray)
        individual_tests(9) = it( &
                "can parse an array with multiple elements", &
                checkParseMultiArray)
        individual_tests(10) = it( &
                "can parse an empty object", checkParseEmptyObject)
        individual_tests(11) = it( &
                "can parse an object with a single member", &
                checkParseSingleObject)
        individual_tests(12) = it( &
                "can parse an object with multiple members", &
                checkParseMultiObject)
        individual_tests(13) = it( &
                "can parse complex data", checkParseExpandedJson)
        tests = describe("parseJson", individual_tests)
    end function test_parse_json

    pure function checkParseEmpty() result(result_)
        type(Result_t) :: result_

        type(ErrorList_t) :: errors
        type(JsonElement_t) :: json

        call parseJson("", errors, json)

        result_ = assertThat(errors%hasAny(), errors%toString())
    end function checkParseEmpty

    pure function checkParseNull() result(result_)
        type(Result_t) :: result_

        type(ErrorList_t) :: errors
        type(JsonElement_t) :: json

        call parseJson(" null ", errors, json)

        result_ = assertNot(errors%hasAny(), errors%toString())
        if (result_%passed()) then
            select type (element => json%element)
            type is (JsonNull_t)
                result_ = succeed("Got null")
            end select
        end if
    end function checkParseNull

    pure function checkParseTrue() result(result_)
        type(Result_t) :: result_

        type(ErrorList_t) :: errors
        type(JsonElement_t) :: json

        call parseJson(" true ", errors, json)

        result_ = assertNot(errors%hasAny(), errors%toString())
        if (result_%passed()) then
            select type (element => json%element)
            type is (JsonTrue_t)
                result_ = succeed("Got true")
            end select
        end if
    end function checkParseTrue

    pure function checkParseFalse() result(result_)
        type(Result_t) :: result_

        type(ErrorList_t) :: errors
        type(JsonElement_t) :: json

        call parseJson(" false ", errors, json)

        result_ = assertNot(errors%hasAny(), errors%toString())
        if (result_%passed()) then
            select type (element => json%element)
            type is (JsonFalse_t)
                result_ = succeed("Got false")
            end select
        end if
    end function checkParseFalse

    pure function checkParseNumber(input) result(result_)
        class(Input_t), intent(in) :: input
        type(Result_t) :: result_

        type(ErrorList_t) :: errors
        type(JsonElement_t) :: json

        select type (input)
        type is (NumberInput_t)
            call parseJson(input%string, errors, json)
            result_ = assertNot(errors%hasAny(), errors%toString())
            if (result_%passed()) then
                select type (element => json%element)
                type is (JsonNumber_t)
                    result_ = assertEquals( &
                            input%value_, &
                            element%getValue(), &
                            "Original string: " // input%string)
                end select
            end if
        class default
            result_ = fail("Expected to get a NumberInput_t")
        end select
    end function checkParseNumber

    pure function checkParseString() result(result_)
        type(Result_t) :: result_

        character(len=*), parameter :: THE_STRING = &
                '"AB\"\\\/\b\n\r\t\u1a2f"'
        type(ErrorList_t) :: errors
        type(JsonElement_t) :: json

        call parseJson(THE_STRING, errors, json)

        result_ = assertNot(errors%hasAny(), errors%toString())
        if (result_%passed()) then
            select type (element => json%element)
            type is (JsonString_t)
                result_ = assertEquals(THE_STRING, element%toCompactString())
            end select
        end if
    end function checkParseString

    pure function checkParseEmptyArray() result(result_)
        type(Result_t) :: result_

        type(ErrorList_t) :: errors
        type(JsonElement_t) :: json

        call parseJson("[ ]", errors, json)

        result_ = assertNot(errors%hasAny(), errors%toString())
        if (result_%passed()) then
            select type (element => json%element)
            type is (JsonArray_t)
                result_ = assertEquals(0, element%length())
            end select
        end if
    end function checkParseEmptyArray

    pure function checkParseSingleArray() result(result_)
        type(Result_t) :: result_

        type(ErrorList_t) :: errors
        type(JsonElement_t) :: json

        call parseJson("[null]", errors, json)

        result_ = assertNot(errors%hasAny(), errors%toString())
        if (result_%passed()) then
            result_ = assertEquals("[null]", json%element%toCompactString())
        end if
    end function checkParseSingleArray

    pure function checkParseMultiArray() result(result_)
        type(Result_t) :: result_

        type(ErrorList_t) :: errors
        type(JsonElement_t) :: json

        call parseJson("[null, null,null]", errors, json)

        result_ = assertNot(errors%hasAny(), errors%toString())
        if (result_%passed()) then
            result_ = assertEquals("[null,null,null]", json%element%toCompactString())
        end if
    end function checkParseMultiArray

    pure function checkParseEmptyObject() result(result_)
        type(Result_t) :: result_

        type(ErrorList_t) :: errors
        type(JsonElement_t) :: json

        call parseJson("{ }", errors, json)

        result_ = assertNot(errors%hasAny(), errors%toString())
        if (result_%passed()) then
            result_ = assertEquals("{}", json%element%toCompactString())
        end if
    end function checkParseEmptyObject

    pure function checkParseSingleObject() result(result_)
        type(Result_t) :: result_

        type(ErrorList_t) :: errors
        type(JsonElement_t) :: json

        call parseJson('{"first" : null}', errors, json)

        result_ = assertNot(errors%hasAny(), errors%toString())
        if (result_%passed()) then
            result_ = assertEquals('{"first":null}', json%element%toCompactString())
        end if
    end function checkParseSingleObject

    pure function checkParseMultiObject() result(result_)
        type(Result_t) :: result_

        type(ErrorList_t) :: errors
        type(JsonElement_t) :: json

        call parseJson( &
                '{"first" : null, "second" : null, "third" : null}', &
                errors, &
                json)

        result_ = assertNot(errors%hasAny(), errors%toString())
        if (result_%passed()) then
            result_ = assertEquals( &
                    '{"first":null,"second":null,"third":null}', &
                    json%element%toCompactString())
        end if
    end function checkParseMultiObject

    pure function checkParseExpandedJson() result(result_)
        type(Result_t) :: result_

        character(len=*), parameter :: EXPANDED_EXAMPLE = &
   '{' // NEWLINE &
// '    "glossary" : {' // NEWLINE &
// '        "title" : "example glossary",' // NEWLINE &
// '        "GlossDiv" : {' // NEWLINE &
// '            "title" : "S",' // NEWLINE &
// '            "GlossList" : {' // NEWLINE &
// '                "GlossEntry" : {' // NEWLINE &
// '                    "ID" : "SGML",' // NEWLINE &
// '                    "SortAs" : "SGML",' // NEWLINE &
// '                    "GlossTerm" : "Standard Generalized Markup Language",' // NEWLINE &
// '                    "Acronym" : "SGML",' // NEWLINE &
// '                    "Abbrev" : "ISO 8879:1986",' // NEWLINE &
// '                    "GlossDef" : {' // NEWLINE &
// '                        "para" : "A meta-markup language, used to create markup languages such as DocBook.",' // NEWLINE &
// '                        "GlossSeeAlso" : [' // NEWLINE &
// '                            "GML",' // NEWLINE &
// '                            "XML"' // NEWLINE &
// '                        ]' // NEWLINE &
// '                    },' // NEWLINE &
// '                    "GlossSee" : "markup"' // NEWLINE &
// '                }' // NEWLINE &
// '            }' // NEWLINE &
// '        }' // NEWLINE &
// '    }' // NEWLINE &
// '}'
        type(ErrorList_t) :: errors
        type(JsonElement_t) :: json

        call parseJson(EXPANDED_EXAMPLE, errors, json)

        result_ = assertNot(errors%hasAny(), errors%toString())
        if (result_%passed()) then
            result_ = assertEquals(EXPANDED_EXAMPLE, json%element%toExpandedString())
        end if
    end function checkParseExpandedJson
end module parse_json_test
