jsonff
=====

[![pipeline status](https://gitlab.com/everythingfunctional/jsonff/badges/master/pipeline.svg)](https://gitlab.com/everythingfunctional/jsonff/commits/master)

JSON for Fortran.

The constants `JSON_NULL`, `JSON_TRUE` and `JSON_FALSE` and the procedures
`JsonNumber`, `JsonString`, `JsonArray`, `JsonMember` and `JsonObject` are
provided for buidling up JSON data structures in Fortran. Note that the procedure
to create a `JsonString` ensures that the string is valid according to the JSON
standard. *Unsafe* versions of the JsonString, JsonMember, and JsonObject
procedures are provided in the event you are certain that you are only dealing
with valid strings.

Once constructed, JSON values can be converted to string representation in
either compact or expanded, human-readable formats. Additionally, procedures are
provided (`parseJson`) that parse a string into a JSON data structure. It also
provides reasonable error messages in the event the string does not contain
valid JSON.

A string generated from a valid JSON data structure is guaranteed to be able to
be parsed by the parser into exactly the same data structure. Note that data
structure parsed from a string is not necessarily guaranteed to produce exactly
the same string, since formatting is not important to JSON data.
